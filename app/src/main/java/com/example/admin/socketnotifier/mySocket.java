package com.example.admin.socketnotifier;

import android.app.Activity;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

/**
 * Created by Admin on 04.07.2016.
 */
public class mySocket {

    private Socket client;

    DataOutputStream dataOutputStream;
    DataInputStream dataInputStream;

    private String address;
    private int port;

    public mySocket(String address, int port) {
        this.address = address;
        this.port = port;
    }

    public void connect() throws IOException {
        client = new Socket(address, port);
        client.setSoTimeout(100);
    }

    public void sendMessage(String message) throws IOException {
        if(client != null) {
            dataOutputStream = new DataOutputStream(client.getOutputStream());
            dataInputStream = new DataInputStream(client.getInputStream());
            dataOutputStream.writeUTF(message);
        }
    }
}
